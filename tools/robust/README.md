**After you have successfully enabled the cami environment variable, type this in the terminal to see the test:**
```bash
python cami_option_1.py data/human_annotated_PPIs_brain.txt data/ms_seeds.txt ms.graphml 0.25 0.9 30 0.1
python cami_option_2.py data/human_annotated_PPIs_brain.txt data/ms_seeds.txt ms.graphml 0.25 0.9 30 0.1
 ```
**Checking for input from the command line:**
- 1 file providing the network in the form of an edgelist
    (tab-separated table, columns 1 & 2 will be used)
- 2 file with the seed genes (if table contains more than one
    column they must be tab-separated; the first column will be
    used only)
- 3 path to output file
- 4 initial fraction
- 5 reduction factor
- 6 number of steiner trees to be computed
- 7 threshold
