# ====================================================
# FileName: cami_37.sh
# Author: Marcos Chow Castro <mctechnology170318@gmail.com>
# GitHub: https://github.com/mctechnology17
# Date: 22.11.2021 20:30
# ====================================================
 
which conda > /dev/null
if test $? -ne 0
then
  echo "$0: conda is not installed. Please first install it"
  exit 1
fi

copyright() {
cat <<END
  Copyright (c) 2021 Marcos Chow Castro

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  SOFTWARE.
END
  exit 1
}

usage(){
cat <<END
  Usage:
                    sh cami_37.sh
                    OR
                    ./cami_37.sh

  If you can't run the script you have to give it the execution permissions:
  Try:
                    chmod +x cami_37.sh
  And finally run:
                   0) Create cami environment:
                      sh cami_37.sh
                      option: y    create cami env
                   1) Initialize cami:
                      conda activate cami
                   2) Update cami:
                      sh cami_37.sh
                      option: u    update env cami
                   3) Install cami certificate:
                      sh cami_37.sh
                      option: ce fix drugstone certificates
                   4) Execute an example:
                      sh cami_37.sh
                      option: ex   execute an example
                   5) Remove tmp file:
                      sh cami_37.sh
                      option: cl   remove tmp file
END
  exit 1
}

menu() {
  echo "===================================="
  echo "|      CAMI ENVIRONMENT INSTALLER  |"
  echo "|----------------------------------|"
  echo "| h    help/usage                  |"
  echo "| y    create cami env             |"
  echo "| a    cami activate               |"
  echo "| u    update env cami             |"
  echo "| ce   install cami certificate    |"
  echo "| ex   execute an example          |"
  echo "| cl   remove tmp file             |"
  echo "| d    cami deactivate             |"
  echo "| s    show env                    |"
  echo "| r    remove cami env             |"
  echo "| c    copyright/permission        |"
  echo "|----------------------------------|"
  echo "|   Press ENTER or CTRL+C to EXIT  |"
  echo "===================================="
  echo "Continue?"
}

install_cami() {
  echo "cami: adding channels"
  conda config --add channels defaults
  conda config --add channels conda-forge
  conda config --add channels bioconda
  echo "cami: creating environment variable with python 3.7"
  conda create --name cami -c conda-forge graph-tool python=3.7
  echo "======================================="
  echo "|    CAMI ENV SUCCESSFULLY CREATED    |"
  echo "|-------------------------------------|"
  echo "| For a complete installation         |"
  echo "| you must follow the following steps |"
  echo "| Initialize cami:                    |"
  echo "|   conda activate cami               |"
  echo "| Update cami:                        |"
  echo "|   sh cami_37.sh                        |"
  echo "|   opt: u    update env cami         |"
  echo "| Install cami certificate:           |"
  echo "|   sh cami_37.sh                        |"
  echo "|   opt: ce fix drugstone certificate |"
  echo "|-------------------------------------|"
}

darwin_certificate() {
  if [ -d $HOME/opt/anaconda3/envs/cami/lib/python3.7/site-packages/certifi/ ]; then
    cat drugstone_certificates.txt >> $HOME/opt/anaconda3/envs/cami/lib/python3.7/site-packages/certifi/cacert.pem
    echo "cami: certificate updated successfully"
  elif [ -d /opt/anaconda3/envs/cami/lib/python3.7/site-packages/certifi/ ]; then
    cat drugstone_certificates.txt >> /opt/anaconda3/envs/cami/lib/python3.7/site-packages/certifi/cacert.pem
    echo "cami: certificate updated successfully"
  else
    echo "cami: the directory certifi in cami_env NOT EXIST"
  fi
}

linux_certificate() {
  if [ -d $HOME/anaconda3/envs/cami/lib/python3.7/site-packages/certifi/ ]; then
    cat drugstone_certificates.txt >> $HOME/anaconda3/envs/cami/lib/python3.7/site-packages/certifi/cacert.pem
    echo "cami: certificate updated successfully"
  elif [ -d /anaconda3/envs/cami/lib/python3.7/site-packages/certifi/ ]; then
    cat drugstone_certificates.txt >> /anaconda3/envs/cami/lib/python3.7/site-packages/certifi/cacert.pem
    echo "cami: certificate updated successfully"
  else
    echo "cami: the directory certifi in cami_env NOT EXIST"
  fi
}

install_certificate() {
case `uname -s` in
  Darwin)  echo "Darwin" && darwin_certificate ;;
  Linux)   echo "Linux" && linux_certificate ;;
esac
}

if [ "$1" = "example" ] ; then
  ./cami/example_run.py
  exit 0
elif [ "$1" = "clean" ] ; then
	rm -r cami/__pycache__
	rm -r data/tmp
  exit 0
fi

remove_cami() {
  clear
  echo "|-------------------------|"
  echo "| Are you sure you want to|"
  echo "| delete cami env   [y/n]?|"
  echo "+-------------------------+"
  echo "| Press CTRL+C to exit    |"
  echo "+-------------------------+"
  read -r cami_del
  if [ "$cami_del" = "y" ]; then
    conda env remove -n cami
  else
    echo "No change made!"
    exit 1
  fi
}

menu
read -r cami_tmp
case $cami_tmp in
  y|Y) install_cami ;;
  h|H) usage ;;
  c|C) copyright ;;
  a|A) conda activate cami || echo "cami: If it was not activated successfully, please try manually: conda activate cami" ;;
  u|U) conda env update cami --file=cami_37_env.yaml; pip install domino-python; pip install pcst_fast;pip install requests_html; pip install biodigest ;;
  d|D) conda deactivate ;;
  s|S) conda info --envs ;;
  r|R) remove_cami ;;
  ce|CE) install_certificate ;;
  ex|EX) ./cami/example_run.py ;;
  cl|CL) rm -r cami/__pycache__; rm -r data/tmp ;;
  *) echo No change made ;;
esac

# vim: set sw=2 ts=2 sts=2 et ft=sh fdm=indent:
