<h1 align="center">Consensus Active Module Identification - CAMI <img src="./src/camilogo_black.jpg" width="30px"></h1>


<div align="center">

  [<img align="center" alt="cami" width="300px" height="200px" src="src/camilogo.jpg" />][cami]

</div>


[Description](#Description) | [Installation](#Installation) | [Usage](#Usage)  | [Directory Tree](#Directory_Tree)| [LICENSE](#LICENSE)

----

## Description

Consensus Active Module Identification(CAMI)

CAMI's main purposes are:
- Consensus Prediction: Use different algorithms to find active disease
  modules in a given PPI-network and combine their results with a consensus function
- Evaluation pipelines: Compare different result sets of algorithms to each other and their consensus using DIGEST and/or a pipeline that estimates the ability to rediscover removed seeds from a given seed set

Input: a protein-protein-interaction-network (PPI-network) and a seed list
Output: multiple sets of predicted Active moduless

Cami was tested with the `python 3.7` interpreter, we recommend using this
version of `python` for a better user experience.
Added channels and modules added to the cami environment variable
are found in the file [cami_env.yml](./cami_env.yaml)
in the root directory of this repository.

## Installation

```bash
git clone https://gitlab.rrz.uni-hamburg.de/bay2046/cami.git
cd cami
```

After successfully cloning the cami repository, follow the steps below for
proper installation and use of cami

```bash
0. Create cami environment:
   sh cami.sh
   option: y    create cami env
1. Initialize cami:
   conda activate cami
2. Update cami:
   sh cami.sh
   option: u    update env cami
3. Execute a example:
   sh cami.sh
   option: ex   execute a example
4. Remove tmp file:
   sh cami.sh
   option: cl   remove tmp file
```

## Usage

```bash
./cami.py [-n] [PPI] [-s] [SEEDS] [-t] [TOOLS] [-id] [IDENTIFIER]
```
### Example
```bash
./cami.py -n ./human_annotated_PPIs_brain.txt  -s ./ms_seeds.txt -t robust -id test_run
```

### CAMI flags
```bash
    -n or --ppi_network  # Path to a csv file containing the different edges
                         # of the base PPI
    -s or -seeds         # Path to a txt file containing the seeds delimitered
                         # by breakline characters
    -t or --tools        # List of tools that the user wants to use
                         # for prediction. Available tools are
                         # domino, diamond, robust, hotnet.
                         # The default tools are: diamond, domino and robust.
    -w or --tool_weights # List of weights for the tools. If you have
                         # [domino, diamond, robust] as list of tools and
                         # diamonds weight should be twice as high as
                         # the other tools type: 1 2 1
    -c or --consensus    # run the consensus prediction part of cami
    -e or --evaluate     # run the evaluation part of cami
    -o or --output_dir   # path to .output directory
    -id or --identifier  # ID for the current excecution of cami.
                         # Defaults to a randomly generated ID
    -tmp or --save_temps # keep temporary files
    -v or --visualize    # Visualize results using Degradome, an external webtool.
                         # Please note that degradome can only be used
                         # for visualization with up to 5 tools.
    -img or --save_image # Save the venn diagram from the visualization as png.
                         # (Only possible for up to 5 tools)
    -f or --forcei       # Ignore warnings and overwrite everything when excecuting CAMI.
    -d or --drugstone    # Visualize the cami module via the drugstone API. If
                         # necessary the user needs to provide a list of the two titles of the two
                         # columns that contain the symbols of the gene edges in the inputfile of the
                         # PPI network. The order needs to correspond to the order of the first two
                         # columns. The default is 'Official_Symbol_Interactor_A
                         # Official_Symbol_Interactor_B'. Please note that the symbol columns cannot
                         # be the first two columns. If the two main edges in the first two columns
                         # are correspond also the gene symbols please duplicate these columns.
    -ncbi or --ncbi      # Save the NCBI URLs and Summaries of the genes in the CAMI output.

```

## Directory Tree
```bash
.
├── bin
├── cami
│  ├── AlgorithmWrapper.py
│  ├── cami.py
│  ├── cami_suite.py
│  ├── degradome.py
│  ├── DiamondWrapper.py
│  ├── DominoWrapper.py
│  ├── drugstone.py
│  ├── example_run.py
│  ├── HHotNetWrapper.py
│  ├── ncbi.py
│  ├── preprocess.py
│  ├── RobustWrapper.py
│  └── test_run.py
├── cami.sh
├── cami_env.yaml
├── CHANGELOG.txt
├── data
│  ├── input
│  ├── output
│  └── tmp
├── doc
│  ├── cami.txt
│  └── tags
├── docker_cami
│  ├── docker-compose.yaml
│  ├── Dockerfile
│  └── README.md
├── drugstone_certificates.txt
├── LICENSE
├── Makefile
├── module_to_exec.txt
├── README.md
├── src
├── tools
│  ├── DIAMOnD
│  ├── diamond_packages.txt
│  ├── HHotNet
│  └── robust
└── tree_cami.txt
```
## [REFERENCES]
The structure of the Prediction Algorithms was derived from https://github.com/dbblumenthal/amim-test-suite
## [LICENSE](LICENSE)

Released under the GNU General Public License v3.0.


[cami]: https://gitlab.rrz.uni-hamburg.de/bay2046/cami
