.PHONY:clean
clean:
	@${RM} -r cami_src/__pycache__
	@${RM} -r data/tmp

.PHONY:example
example:
	@echo 'Running CAMI with example seeds and network.'
	@./cami_src/example_run.py