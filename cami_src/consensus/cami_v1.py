import sys, os

def run_cami(result_sets, ppi_graph, seed_lst, predicted_by, cami_scores, code2toolname, tool_code, params):
    consens_threshold = params['consens_threshold']
    # calculate gene weights
    # set of all result genes
    cami_vertices = set()
    putative_vertices = set()
      # CONFIG: consensus_threshold = 2

    # parse every result set of each tool
    for tool in result_sets:
        #print(f'{tool.name}: {tool.weight}')

        # TODO: Should we keep the seeds in the result sets?
        # everytime a tool predicted a gene add 1 * the tool weight to its weight and add it to the result genes
        for vertex in result_sets[tool]:
            predicted_by[vertex][tool.code] = 1  # TODO: tool or tool.name?
            cami_scores[vertex] += 1.0 * tool.weight
            putative_vertices.add(vertex)
            if cami_scores[
                vertex] >= consens_threshold:  # if a vertex was predicted twice (or once if there is only 1 tool used) add it to the cami set
                putative_vertices.remove(vertex)
                cami_vertices.add(vertex)
                predicted_by[vertex][tool_code] = 1

    # TODO: Find alternate ways to calculate CAMI scores => The heavy weights should get +0.5 too?
    # TODO: Try to rerun cami with varying input seeds?
    # add a putative gene to the cami set when it is in the neighborhood of the existing cami genes or the seed genes
    heavy_vertices = cami_vertices.copy()
    for seed in seed_lst:
        heavy_vertices.add(seed)

    for vertex in heavy_vertices:
        neighbors = vertex.all_neighbors()
        for vertex in putative_vertices:
            if vertex in neighbors:  # if a vertex is in the neighborhood of the heavy vertices increase the cami_score
                cami_vertices.add(vertex)
                predicted_by[vertex][tool_code] = 1
                cami_scores[vertex] += 0.5



    # translate tool code to string
    codes2tools = {vertex: [code2toolname[idx] for idx, code in enumerate(predicted_by[vertex]) if code == 1] for
                   vertex in ppi_graph.vertices()}

    return cami_vertices, putative_vertices, codes2tools

def make_union(result_sets, ppi_graph, seed_list,
               predicted_by, cami_scores,
               code2toolname, tool_code, params):
    codes2tools = {vertex: [code2toolname[idx] for idx, code in enumerate(predicted_by[vertex]) if code == 1] for
                   vertex in ppi_graph.vertices()}
    putative_vertices = set()
    union = set()
    for tool in result_sets:
        for vertex in result_sets[tool]:
            putative_vertices.add(vertex)
            union.add(vertex)
    return union, putative_vertices, codes2tools

def make_intersection(result_sets, ppi_graph, seed_list,
               predicted_by, cami_scores,
               code2toolname, tool_code, params):
    codes2tools = {vertex: [code2toolname[idx] for idx, code in enumerate(predicted_by[vertex]) if code == 1] for
                   vertex in ppi_graph.vertices()}
    putative_vertices = set()
    intersect = set(list(result_sets.values())[0])
    for tool in result_sets:
        intersect = intersect.intersection(result_sets[tool])
    return intersect, putative_vertices, codes2tools

def make_first_neighbor_result_set(result_sets, ppi_graph, seed_list,
                                   predicted_by, cami_scores,
                                   code2toolname, tool_code, params):
    codes2tools = {vertex: [code2toolname[idx] for idx, code in enumerate(predicted_by[vertex]) if code == 1] for
                   vertex in ppi_graph.vertices()}
    putative_vertices = set()
    first_neighbor = set()
    for seed in seed_list:
        for neighbor in seed.all_neighbors():
            first_neighbor.add(neighbor)
    return first_neighbor, putative_vertices, codes2tools