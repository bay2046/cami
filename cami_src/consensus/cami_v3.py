import sys
from collections import defaultdict
from utils.networks import trustrank, betweenness, must, closeness
import graph_tool as gt


# This uses a trustrank algorithm to rank all putative nodes starting from the seeds and only accepts the top 0.X entries
# TODO maybe find a smart way to cutoff automatically?
def run_cami(result_sets, ppi_graph, seed_lst, predicted_by, cami_scores, code2toolname, tool_code, params):
    hub_penalty = params['hub_penalty']
    confidence_level = params.get('confidence_level', 0.5)
    weighted = 'weighted' in params and params['weighted']
    ranking_method = params['ranking'] if 'ranking' in params else 'trustrank'
    trees = params.get('trees', 5)
    tolerance = params.get('tolerance', 10)

    # calculate gene weights
    # set of all result genes
    cami_vertices = set()
    putative_vertices = set()
    # CONFIG: consensus_threshold = 2
    # parse every result set of each tool
    counts = defaultdict(lambda: 0)
    for tool in result_sets:
        for vertex in result_sets[tool]:
            putative_vertices.add(vertex)
            counts[vertex] = counts[vertex] + tool.weight
        for vertex in seed_lst:
            counts[vertex] = counts[vertex] + tool.weight

    tool_scores = dict()
    for tool in result_sets:
        subnet = gt.GraphView(ppi_graph, vfilt=lambda v: v in result_sets[tool] or v in seed_lst)
        weights = None
        if weighted:
            weights = subnet.new_edge_property("double")
            for v, c in counts.items():
                weights.a[int(v)] = c

        if ranking_method == 'trustrank':
            damping_factor = params['damping_factor']
            scores = trustrank(subnet, seed_lst, damping_factor, hub_penalty, weights)
        elif ranking_method == 'betweenness':
            scores = betweenness(subnet, hub_penalty, weights)
        elif ranking_method == 'must':
            scores = must(subnet, seed_lst, trees, hub_penalty, weights, tolerance)
        elif ranking_method == 'harmonic':
            scores = closeness(subnet, hub_penalty, weights)
        tool_scores[tool] = scores

    putative_score_map = defaultdict(lambda: 0)
    for _, scores in tool_scores.items():
        for id in putative_vertices:
            try:
                putative_score_map[id] += scores.a[int(id)]
            except:
                pass
    putative_scores = list(putative_score_map.values())
    putative_scores.sort()
    putative_scores.reverse()
    threshold = putative_scores[int(len(putative_vertices) * (1 - confidence_level))]
    for v in putative_vertices:
        if putative_score_map[v] >= threshold and putative_score_map[v] > 0:
            cami_vertices.add(v)
            predicted_by[v][tool_code] = 1

    # translate tool code to string
    codes2tools = {vertex: [code2toolname[idx] for idx, code in enumerate(predicted_by[vertex]) if code == 1] for
                   vertex in ppi_graph.vertices()}
    return cami_vertices, putative_vertices, codes2tools
