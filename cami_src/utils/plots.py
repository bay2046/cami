import matplotlib.pyplot as plt

def make_violinplot(value_dct, colors, color_groups, title, ylabel, ax):
    # Create a plot
    groups = list(value_dct.keys())
    violins = ax[0].violinplot(list(value_dct.values()), showmeans=True, showextrema=True)

    for violinpart in list(violins.keys())[2:]:
        violins[violinpart].set_color('k')

    for violin, group in zip(violins['bodies'], groups):
        for i, color_group in enumerate(color_groups):
            if group in color_group:
                violin.set_facecolor(colors[i])
                break
    # Add title
    ax.set_title(title, wrap=True, fontsize=14)

    ax.set_xticks(list(range(1, len(value_dct) + 1)))
    ax.set_xticklabels(value_dct.keys(), wrap=True)
    ax.tick_params(axis='x', labelsize=11)
    ax.set_ylabel(ylabel, wrap=True, fontsize=14)
    return ax