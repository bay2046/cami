from scipy.stats import kstest

def calculate_ks_p_value(data1, data2):
    """Calculate the Kolmogorov-Smirnoff p-value for two data sets.

    Parameters
    ----------
    data1 : array-like
        The first data set.
    data2 : array-like
        The second data set.

    Returns
    -------
    p_value : float
        The p-value for the Kolmogorov-Smirnoff test.
    """
    return kstest(data1, data2)[1]