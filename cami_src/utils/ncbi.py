from time import sleep
import requests,re


# This whole function takes about 2.5 seconds so no danger of being blocked from ncbi :D
def send_request(gen_id):
    url = f"https://www.ncbi.nlm.nih.gov/gene/{gen_id}"
    r = requests.get(url)
    response = r.text
    summary_match = re.search(r'(<dt>Summary</dt>\n)(.*)(<dd>)(.*)(</dd>)', response)
    if summary_match:
        summary = summary_match.group(4)
        return url, summary
    return url, None

if __name__ == '__main__':
    import time
    start_time = time.time()
    send_request('6850')
    print("--- %s seconds ---" % (time.time() - start_time))