import requests

def send_request(result_sets):
    url = 'api.digest-validation.net/set_set'

    node_lst=[{'id':symbol} for symbol in nodes]

    edge_lst = [{'from':edge[0], 'to':edge[1]} for edge in edges]

    data = {'network': {'nodes' : node_lst,
                        'edges' : edge_lst
                       }
            }

    r = requests.post(url, json=data)
    id = r.text.strip('"')
    result_url = f'https://drugst.one?id={id}'
    get_r = requests.get(result_url)
    return get_r.url

if __name__ == '__main__':
    send_request(['ABCD', 'EFGH', 'IJKL', 'MNOP'], [('ABCD', 'EFGH'), 
                 ('IJKL', 'EFGH'), ('MNOP', 'ABCD')])
