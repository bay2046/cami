import requests, re

def send_request(sets=dict(), seeds=list()):
    url = 'http://degradome.uniovi.es/cgi-bin/nVenn/nvenn.cgi'

    groups = '['
    for tool in sets:
        groups += '{' + f'"name":"{tool}","els":['
        for node in sets[tool]:
            if node not in seeds:
                groups += f'"{node}",'
        groups = groups[:-1]
        groups += ']},'
    groups = groups[:-1]
    groups += ']'

    values = {
              'groups': groups,
              'header': 'rawData'
             }
    print(f'Sending result sets to {url} to create Venn diagrams...')
    r = requests.post(url, data=values)
    if r.ok:
        match = re.search(r'(<a href=\\")(http:\/\/degradome.*)(\\">)', r.text)
        if match:
            url = match.group(2)
            print(f'created Venn diagrams using degradome. You can find the visualization via:{url}')
            return url
        else:
            print('Degradome failed to create Venn diagrams')
            exit(1)
    else:
        print('Degradome sent no response.')
        exit(2)
        
def download_image(degradome_url, file):
    import urllib.request
    from requests_html import HTMLSession
    session = HTMLSession()
    response = session.get(degradome_url)
    response.html.render(timeout=20, sleep=3)
    link_list = response.html.absolute_links
    session.close()
    for link in link_list:
        if 'data:image/png;' in link:
            urllib.request.urlretrieve(link, file)
            print(f'Venn diagram image saved under: {file}')
            return response
    print(f'Something went wrong. Could not automatically download the venn diagram. Please try it manually under: {degradome_url}')
