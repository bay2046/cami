from algorithms.AlgorithmWrapper import AlgorithmWrapper
import subprocess

from configparser import ConfigParser

class CustomWrapper(AlgorithmWrapper):
    def __init__(self,config):
        """Wrapper that allows to use results from any algorithm that is not included in the CAMI suite yet
           has a class variable 'external_prediction_path' that points to the path of the external prediction file
        """
        super().__init__(config)
        self.name = 'custom_algorithm'
        self.code = -1
        self.external_prediction_path = 'path/to/external/prediction/file'
        ppi_vertex2gene = self.ppi_graph.vertex_properties["name"]
        self.ppi_gene2vertex = {ppi_vertex2gene[vertex]: vertex for vertex in self.ppi_graph.vertices()}

    def run_algorithm(self, inputparams):
        """extract the output from the external prediction file, that contains the name of the external tool in the first line
           set the name for the algorithm to the name of the external tool
        Args:
            inputparams (list): A list of parameters for the algorithm that is defined via
                                prepare_input()

        Returns:
            list: A list of resulting genes extracted from the generated output file
        """
        external_prediction_file = inputparams[0]
        result_list = []
        with open(external_prediction_file) as rfile:
            for idx, line in enumerate(rfile):
                if idx == 0:
                    self.name = line.strip()
                else:
                    node = line.strip()
                    if node in self.ppi_gene2vertex:
                        result_list.append(self.ppi_gene2vertex[node])
        return result_list

    def prepare_input(self):
        """this function returns the path to the external prediction file
        """
        return self.external_prediction_path

    def extract_output(self, algo_output):
        """this function does not do anything in the case of an external algorithm
        """
        pass
    