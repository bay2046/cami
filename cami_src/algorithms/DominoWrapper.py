from algorithms.AlgorithmWrapper import AlgorithmWrapper
from graph_tool import Graph
import subprocess, os, re, preprocess
# conda install -c conda-forge graph-tool

#MC:
from configparser import ConfigParser
import ast

class DominoWrapper(AlgorithmWrapper):
    def __init__(self, config):
        super().__init__(config)
        self.name = 'DOMINO'
        self.code = 2
        config = ConfigParser()
        config.read(self.config)

        self.visualization_flag = config.get('domino', 'visualization_flag')
        self.output_name = config.get('domino', 'output_name')
        self.parallels = config.get('domino', 'para')
        self.c = config.get('domino', 'c')

    def run_algorithm(self, inputparams):
        """Run Domino algorithm

        :param inputparams: list of inputparameters, in this case the paths to
                            the ppi and seeds
        :type inputparams: list(str)
        :return: list of predicted nodes
        :rtype: list(str)
        """
        ppi = inputparams[0]

        assert os.path.exists(ppi), f"Could not find PPI-network file {ppi}"

        seeds = inputparams[1]

        assert os.path.exists(ppi), f"Could not find seed file {seeds}"

        slices_file = inputparams[2]

        #MC:
        #CONFIG: visualization_flag = False
        command = f'domino -a "{seeds}" -n "{ppi}" -s "{slices_file}" \
            -o "{self.output_dir}" -v {self.visualization_flag} -p {self.parallels} --use_cache {self.c}'
        run = subprocess.run(command, shell=True, capture_output=True)
        match = re.search("( final modules are reported at )(.*)(\n)", run.stdout.decode('utf-8'))
        assert(match!=None), 'DOMINO could not find any modules'
        algo_output = match.group(2)
        #MC:
        #CONFIG output_name = 'modules.out'
        assert os.path.exists(algo_output), f'Could not create output file {algo_output} for domino'
        outputfilename = self.name_file('out', 'out')
        command = f'mv "{algo_output}" "{os.path.join(self.output_dir, outputfilename)}"'
        subprocess.call(command, shell=True, stdout=subprocess.PIPE)
        algo_output = os.path.join(self.output_dir, outputfilename)
        if self.debug: print(f"{self.name} results saved in {algo_output}")

        return self.extract_output(algo_output)

    def prepare_input(self):
        """prepares the input ppi and seed genes as needed by the DOMINO:
           ppi_network in as .sif file of the tabular form: node1 edge_type node2
        """
        inputparams = []
        # prepare inputfiles
        if self.debug: print(f'creating {self.name} input files in {self.output_dir}')

        ppi_filename = self.name_file('ppi', 'sif')
        ppi_file = os.path.join(self.output_dir, ppi_filename)

        seed_filename = self.name_file('seeds')
        seed_file = os.path.join(self.output_dir, seed_filename)

        # create the ppi_file which contains all edges in the ppi_network
        with open(ppi_file, "w") as file:
            file.write('node1\tppi\tnode2\n')
            edges = list(self.ppi_network.edges())
            for edge in edges:
                file.write(f"{str(edge.source()) + '_'}\tppi\t{str(edge.target()) + '_'}\n")
                # the nodes need to be appended by '_' so that pandas recognizes the vertices as strings
        inputparams.append(ppi_file)
        if self.debug: print(f'{self.name} ppi is saved in {ppi_file}')

        with open(seed_file, "w") as file:
            file.write('#node\n')
            for seed in self.seeds:
                file.write(f"{seed}_\n")
        inputparams.append(seed_file)
        if self.debug: print(f'{self.name} seeds are saved in {seed_file}')

        slices_filename = self.name_file('slices')
        slices_output = os.path.join(self.output_dir, slices_filename)

        if not os.path.exists(slices_output):
            if self.debug: print('creating domino slices_file...')
            command = f'slicer --network_file "{ppi_file}" --output_file "{slices_output}"'
            subprocess.call(command, shell=True, stdout=subprocess.PIPE)
        if self.debug: print(f'{self.name} slices are saved in {slices_output}')
        inputparams.append(slices_output)
        return inputparams

    def extract_output(self, algo_output):
        nodes = []
        with open(algo_output, "r") as output:
            for line in output:
                for node in re.findall(r'[0-9A-Za-z]+_', line):
                    nodes.append(int(node[:-1]))
        return nodes
