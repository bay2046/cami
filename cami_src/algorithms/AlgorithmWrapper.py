from graph_tool import Graph
import os
from configparser import ConfigParser

from numpy import outer

class AlgorithmWrapper(object):
    #TODO: temporary/Working directory als parameter?
    """Abstract wrapper class for the network enrichment algorithms used in the tests."""
    def __init__(self, config):
        self.uid = '0000'
        self.name = ''
        self.weight = 1
        self.output_dir = ''
        self.ppi_network = Graph()
        self.seeds = list()
        self.home_path = ''
        self.config = config
        self.code = 99
        self.debug = False
        
    def set_weight(self, weight=None):
        if weight == None:
            config = ConfigParser()
            config.read(self.config)
            self.weight = float(config.get(str(self.name).lower(), 'toolweight'))
        else:
            self.weight = weight
            
    def set_config(self, config_file):
        self.config = config_file
        
    def set_ppi_network(self, graph):
        self.ppi_network = graph

    def set_seeds(self, seeds):
        self.seeds = seeds

    def set_id(self, uid):
        self.uid = uid

    def set_homepath(self, path):
        self.home_path = os.path.abspath(path)

    def create_tmp_output_dir(self, tmp_dir):
        out_dir = os.path.join(tmp_dir, self.name)
        #print(tmp_dir)
        if not os.path.exists(out_dir):
            os.mkdir(out_dir)
            if self.debug: print(f"created temporary directory for {self.name} named {out_dir}...")
        self.output_dir = out_dir

    def name_file(self, kind, ending='txt'):
        return f'{self.name}_{kind}.{ending}'

    def run_algorithm(self, inputparams):
        """runs a module identification algorithm

        :param inputparams: list of parameters as needed by the algorithm
        :type inputparams: list(str)
        :param identifier: indetifier for output files
        :type identifier: str
        :return: list of predicted nodes
        :rtype: lst(str)
        """
        pass

    def prepare_input(self):
        """prepares the input ppi and seed genes as needed by the algorithm

        :param ppi_network: the original ppi-network
        :type ppi_network: Graph
        :param seed_genes: list of seed genes for the algorithm
        :type seed_genes: lst(str)
        :param identifier: indetifier for output files
        :type identifier: str
        :return: inputparameters for the algorithm
        :rtype: list(str)
        """
        pass

    def extract_output(self, algo_output):
        """return the output as a list of nodes predicted by an algorithm

        :param algo_output: path to outputfile as given by the algorithm
        :type algo_output: str
        :param identifier: indetifier for output files
        :type identifier: str
        :return: list of predicted nodes
        :rtype: lst(str)
        """
        pass
