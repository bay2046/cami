from algorithms.AlgorithmWrapper import AlgorithmWrapper
import subprocess, os

#MC:
from configparser import ConfigParser
import ast

class DiamondWrapper(AlgorithmWrapper):
    def __init__(self, config):
        super().__init__(config)
        self.name = 'DIAMOnD'
        self.code = 1
        config = ConfigParser()
        config.read(self.config)
        self.alpha = int(config.get('diamond', 'alpha'))
        self.pred_factor = int(config.get('diamond', 'pred_factor'))
        self.max_preds = int(config.get('diamond', 'max_preds'))
        self.p_value_cutoff = float(config.get('diamond', 'p_value_cutoff'))

    def run_algorithm(self, inputparams):
        """Run DIAMOnD algorithm

        :param inputparams: list of inputparameters, in this case the paths to
                            the ppi and seeds
        :type inputparams: list(str)
        :return: list of predicted nodes
        :rtype: list(str)
        """
        # Run DIAMOnD.

        # path to diamond
        diamond_path = os.path.join(self.home_path, 'tools/DIAMOnD/')
        diamond = f'cd "{diamond_path}"; python DIAMOnD.py'
        ppi = inputparams[0] # path to ppi inputfile
        seeds = inputparams[1] # path to seed inputfile
        nof_predictions = inputparams[2] # how many active genes should be predicted

        out_filename = self.name_file('out') # name the outputfile
        algo_output = os.path.join(self.output_dir, out_filename) # specify the output location
        #MC:
        #CONFIG alpha = 1
        command = f'{diamond} "{ppi}" "{seeds}" {nof_predictions} {self.alpha} "{algo_output}"'
        subprocess.call(command, shell=True, stdout=subprocess.PIPE)
        assert os.path.exists(algo_output), f'DIAMOnD failed to save output to {algo_output}'
        if self.debug: print(f"DIAMOnD results saved in {algo_output}")
        return self.extract_output(algo_output)

    def prepare_input(self):
        """prepares the input ppi and seed genes as needed by the algorithm
        """
        inputparams = []
        # prepare inputfiles
        # name ppi and seed file, specify path to files
        ppi_filename = self.name_file('ppi')
        ppi_file = os.path.join(self.output_dir, ppi_filename)
        seed_filename = self.name_file('seeds')
        seed_file = os.path.join(self.output_dir, seed_filename)

        # create ppi file
        with open(ppi_file, "w") as file:
            # parse through the ppi graph and write the ids of the vertices into a file
            for edge in self.ppi_network.edges():
                file.write(f"{str(edge.source())},{str(edge.target())}\n")
        inputparams.append(ppi_file)
        assert os.path.exists(ppi_file), f'Could create PPI-network file "{ppi_file}"'
        if self.debug:
            print(f'{self.name} ppi is saved in {ppi_file}')

        # create seed file
        # parse through the seed list and write the ids of the vertices into a file
        with open(seed_file, "w") as file:
            for seed in self.seeds:
                file.write(f"{seed}\n")
        assert os.path.exists(seed_file), f'Could create seed file "{seed_file}"'
        if self.debug:
            print(f'{self.name} seeds are saved in {seed_file}')
        inputparams.append(seed_file)

        # do not predict too much when there are not enough seeds
        nof_seeds = len(self.seeds)
        #MC:
        #CONFIG pred_factor = 10, max_preds = 100
        nof_preds = min([nof_seeds * self.pred_factor, self.max_preds])
        if self.debug:
            print(f'With {nof_seeds} seeds, {self.name} will try to predict {nof_preds} active modules.')
        inputparams.append(nof_preds)
        return inputparams

    def extract_output(self, algo_output):
        """extract the results from an outputfile to a list indices

        :param algo_output: path to outputfile
        :type algo_output: str
        :return: list of indicesb
        :rtype: list(int)
        """
        nodes = []
        with open(algo_output, "r") as output:
            for line in output.readlines()[1:]:
                l = line.split('\t')
                node,pvalue = l[1], float(l[2])
                if pvalue < self.p_value_cutoff:
                    nodes.append(int(line.split("\t")[1]))
        return nodes
