#!/usr/bin/env python3
import pwd
import sys
from os import chdir
import subprocess


chdir((sys.argv[0].rsplit('/', 1))[0])
networkfile = "../data/input/networks/example_network.tsv"
seedfile = "../data/input/seeds/example_seeds.txt"
seedfile = "../data/input/seeds/multiple_sclerosis.tsv"
identifier = "example_run"
command = f'./cami.py -n {networkfile} -s {seedfile} -id {identifier} -p -f -v;'
subprocess.call(command, shell=True)