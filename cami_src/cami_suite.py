import sys
import threading, biodigest, os
import uuid
from utils import drugstone, degradome, ncbi, comparison_matrix
from algorithms.DiamondWrapper import DiamondWrapper
from algorithms.DominoWrapper import DominoWrapper
from algorithms.RobustWrapper import RobustWrapper
from configparser import ConfigParser
import preprocess
from consensus import cami_v1, cami_v2, cami_v3
import matplotlib.pyplot as plt
import itertools


def generate_param_combinations(params_dict):
    """
    Generates all possible combinations of parameters for the given function(s) and returns them as a list.

    Args:
        params_dict (dict): A dictionary containing the parameters and their possible values for each function.

    Returns:
        list: A list of lists where each inner list contains the function name, parameter string, and a dictionary with the parameter values and corresponding function.
    """
    params_keys = list(params_dict.keys())
    params_values = [params_dict[k] for k in params_keys if k != 'function']
    function_dict = params_dict['function']
    function_names = list(function_dict.keys())
    result = []
    for function_name, function in function_dict.items():
        param_combinations = itertools.product(*params_values)
        for combination in param_combinations:
            param_dict = dict(zip(params_keys[:-1], combination))
            params_str = '_'.join([f"{k}_{v}".replace(' ', '') for k, v in param_dict.items()])
            result.append([function_name, params_str, {'params': param_dict, 'function': function}])
    return result


def initialize_cami(path_to_ppi_file=''):
    cami_params = {}
    # find homepath aka ~/cami
    current_wd = os.getcwd()
    current_wd_lst = current_wd.rsplit('/', 1)
    current_folder = current_wd_lst[-1]
    while current_folder != 'cami':
        os.chdir(current_wd_lst[0])
        current_wd = os.getcwd()
        current_wd_lst = current_wd.rsplit('/', 1)
        current_folder = current_wd_lst[-1]
    home_path = current_wd
    cami_params['home_path'] = home_path
    cami_source = os.path.join(home_path, 'cami_src')
    cami_params['cami_src_path'] = cami_source

    # initialize tool wrappers
    diamond = DiamondWrapper()
    domino = DominoWrapper()
    robust = RobustWrapper()
    wrappers = [diamond, domino, robust]
    nof_tools = len(wrappers)
    cami_params['tool_wrappers'] = wrappers

    # preprocessing
    if path_to_ppi_file == '':
        ppi_network = 'example_network.tsv'
        ppi_file = os.path.join(home_path, f'data/input/networks/{ppi_network}')
    else:
        ppi_file = path_to_ppi_file

    symbol_columns = []  # if the two symbol columns in the ppi_network file are not named
    # ['Official_Symbol_Interactor_A', 'Official_Symbol_Interactor_B']
    # provide the names here.
    ppi_graph = preprocess.csv2graph(ppi_file, symbol_columns, nof_tools)

    cami_params['ppi_graph'] = ppi_graph

    # dictionary with name of the seeds and file 
    seed_directory = os.path.join(home_path, 'data/input/seeds')
    seed_files = {'adhd': 'adhd.tsv', \
                  'alcl': 'alcl.tsv', \
                  'joubert': 'joubert_syndrome.tsv'}
    seed_paths = {}
    for seed_file in seed_files:
        seed_paths[seed_file] = os.path.join(seed_directory, seed_files[seed_file])

    seed_lists = {seedname: preprocess.txt2lst(seed_paths[seedname]) for seedname in seed_paths}


class cami():
    """ A module that is used for Active Module identifaction based on a
        consensus approach
    """

    def __init__(self, ppi_graph, seed_lst, tool_wrappers, home_path, initial_seed_lst, uid=None, output_dir='',
                 configuration='camiconf',
                 parallelization=False, ncbi=False, debug=False, save_temps=False, toolweights=None):
        """Instance variables of CAMI

        :param ppi_graph: The PPI-Graph on which all predictions in CAMI are based of
        :type ppi_graph: Graph()
        :param seed_lst: A list of vertices that are the seeds for the predictions
        :type seed_lst: Graph().vertex()
        :param tool_wrappers: A list of AlgorithmWrappers() that correspond to the tools used for the predictions
        :type tool_wrappers: list(AlgorithmWrapper())
        :param output_dir: The path to the directory where the results are supposed to be saved
        :type output_dir: str
        :param uid: Identifier for the current excecution of CAMI
        :type uid: str
        :param tmp_dir: Directory where temporary files should be saved
        :type tmp_dir: str
        :param home_path: Path to the cami home directory (gitlab repository)
        :type home_path: str
        """
        self.debug = debug
        self.ppi_graph = ppi_graph
        self.origin_ppi_graph = ppi_graph.copy()
        self.ppi_vertex2gene = self.ppi_graph.vertex_properties["name"]
        self.ppi_gene2vertex = {self.ppi_vertex2gene[vertex]: vertex for vertex in self.ppi_graph.vertices()}
        self.initial_seed_lst = initial_seed_lst
        self.seed_lst = seed_lst
        self.origin_seed_lst = seed_lst.copy()
        self.tool_wrappers = tool_wrappers
        self.prediction_tools = [wrapper.name for wrapper in tool_wrappers]
        self.toolweights = toolweights
        self.home_path = home_path
        if uid == None:
            uid = str(uuid.uuid4())
        self.uid = str(uid)
        if output_dir == None:
            output_dir = os.path.join(home_path, 'data', 'output', self.uid)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        if self.debug:
            print(f"Output directory of cami: {output_dir}")
        self.output_dir = output_dir

        tmp_dir = os.path.join(home_path, 'data', 'tmp', self.uid)
        if not os.path.exists(tmp_dir):
            os.makedirs(tmp_dir)
        if debug:
            print(f'Creating unique temporary directory for CAMI: {tmp_dir}')
        self.tmp_dir = tmp_dir

        self.nof_tools = len(tool_wrappers)
        self.result_gene_sets = {}  # contains the genes predicted by the tools (not the indices) WITHOUT seeds
        self.result_module_sets = {}  # contains the genes predicted by the tools (not the indices) WITH seeds
        self.cami_module = []  # TODO: pick place where cami_module is set, which consensus approach should we use?
        self.code2toolname = {tool.code: tool.name for tool in self.tool_wrappers}
        self.code2toolname[0] = 'No tool'
        self.ncbi = ncbi

        config = ConfigParser()
        config.read(configuration)
        self.seed_score = config.get('cami', 'seed_score')
        self.config = configuration
        self.threaded = parallelization
        # set weights for seed genes in ppi_graph
        for seed in self.seed_lst:
            self.ppi_graph.vertex_properties["cami_score"][seed] = self.seed_score
        # initialize tools
        for tool in tool_wrappers:
            self.initialize_tool(tool)

    def reset_cami(self, new_uid='', change_tmp=False):
        if not new_uid == '':
            self.uid = new_uid
        if change_tmp:
            new_tmp_dir = os.path.join(self.home_path,
                                       'data',
                                       self.uid)
            os.makedirs(new_tmp_dir)
            self.tmp_dir = new_tmp_dir
        self.ppi_graph = self.origin_ppi_graph.copy()
        self.result_gene_sets = {}
        self.result_module_sets = {}
        self.cami_vertices = []
        self.seed_lst = self.origin_seed_lst.copy()
        self.code2toolname = {tool.code: tool.name for tool in self.tool_wrappers}
        self.code2toolname[0] = 'No tool'

    def set_initial_seed_lst(self, seedlst):
        self.initial_seed_lst = seedlst

    def initialize_tool(self, tool):
        tool.set_ppi_network(self.ppi_graph)
        tool.set_seeds(self.seed_lst)
        tool.set_homepath(self.home_path)
        tool.set_id(self.uid)
        tool.set_config(self.config)
        if self.toolweights is not None:
            tool.set_weight(self.toolweights[tool.code - 1])
        else:
            tool.set_weight()

    def initialize_all_tools(self):
        for tool in self.tool_wrappers:
            self.initialize_tool(tool)

    def run_tool(self, tool):
        """Excecute the predictions using the AlgorithmWrapper() of a tool

        :param tool: A tool that has the following methods: prepare_input, run_tool() and extract_output()
        :type tool: AlgorithmWrapper()
        :return: A set of predicted vertices by the used tool
        :rtype: set()
        """
        tool.create_tmp_output_dir(self.tmp_dir)  # creates the temporary input directory
        if self.debug: print(f"preparing {tool.name} input...")
        inputparams = tool.prepare_input()
        if self.debug: print(f'running {tool.name}...')
        preds = set(tool.run_algorithm(inputparams))
        if self.debug:
            print(f'{tool.name} predicted {len(preds)} active vertices (seeds not excluded):')
            print(preds)
        return preds

    def make_evaluation(self):
        seed_gene_lst = [self.ppi_vertex2gene[seed] for seed in self.seed_lst]
        ppi_graph_file = os.path.join(self.tmp_dir, f'ppi_graph_{self.uid}.graphml')
        self.ppi_graph.save(ppi_graph_file)

        biodigest.setup.main(setup_type="api")
        for result_set in self.result_module_sets:
            set_validation_results = biodigest.single_validation.single_validation(
                tar=set(self.result_module_sets[result_set]),
                tar_id='entrez',
                mode='set-set',
                distance='jaccard',
                ref=set(seed_gene_lst),
                ref_id='entrez')

            if set_validation_results['status'] == 'ok':
                biodigest.single_validation.save_results(set_validation_results, f'{result_set}_{self.uid}',
                                                         self.output_dir)
                biodigest.evaluation.d_utils.plotting_utils.create_plots(results=set_validation_results,
                                                                         mode='set-set',
                                                                         tar=set(self.result_module_sets[result_set]),
                                                                         tar_id='entrez',
                                                                         out_dir=self.output_dir,
                                                                         prefix=f'{result_set}_{self.uid}',
                                                                         file_type='png')

                with open(os.path.join(self.tmp_dir, f'{result_set}_{self.uid}_relevance_scores.tsv'), 'w') as f:
                    rel_score_name = list(set_validation_results['input_values']['values'].keys())[0]
                    f.write(f'value\t{rel_score_name}\n')
                    val_res_dct = set_validation_results['input_values']['values'][rel_score_name]
                    for val in val_res_dct:
                        f.write(f'{val}\t{val_res_dct[val]}\n')

            # sub_validation_results = biodigest.single_validation.single_validation(
            #     tar=set(self.result_module_sets[result_set]),
            #     tar_id='entrez',
            #     mode='subnetwork-set',
            #     distance='jaccard',
            #     network_data={"network_file":ppi_graph_file,
            #                   "prop_name":"name",
            #                   "id_type":"entrez"},
            #     ref=set(seed_gene_lst),
            #     ref_id='entrez'
            #     )
            # if sub_validation_results['status'] == 'ok':
            #     biodigest.single_validation.save_results(sub_validation_results, f'{result_set}_{self.uid}',
            #                                              self.output_dir)
            #     biodigest.evaluation.d_utils.plotting_utils.create_plots(results=sub_validation_results,
            #                                                              mode='subnetwork-set',
            #                                                              tar=set(self.result_module_sets[result_set]),
            #                                                              tar_id='entrez',
            #                                                              out_dir=self.output_dir,
            #                                                              prefix=f'{result_set}_{self.uid}',
            #                                                              file_type='png')

    def run_threaded_tool(self, tool, pred_sets):
        """run a tool in one thread and save the results into a dictionary pred_sets

        Args:
            tool (AlgorithmWrapper): Wrapper class for a tool
            pred_sets (dict): a dictionary that maps a tool to its result set
        """
        preds = self.run_tool(tool)
        pred_sets[tool] = preds  # - seed_set

    def make_predictions(self) -> dict:
        """create all predictions using the tools specified in tool_wrappers

        :return: A dictionary that saves the predicted vertices with respect
                 to the corresponding tool
        :rtype: dict(AlgorithmWrapper():set(Graph.vertex()))
        """
        if self.debug:
            print(f'Creating result sets of all {self.nof_tools} tools...')
        pred_sets = {tool: None for tool in self.tool_wrappers}

        if self.threaded:
            threads = [threading.Thread(target=self.run_threaded_tool, args=(tool, pred_sets,))
                       for tool in self.tool_wrappers]
            for thread in threads:
                thread.start()

            for thread in threads:
                thread.join()
        else:
            for tool in self.tool_wrappers:
                pred_sets[tool] = self.run_tool(tool)

        assert (list(pred_sets.values()).count(None) < 1), "One of the tools did not return a result set"
        result_sets = {tool: set([self.ppi_graph.vertex(idx) for idx in pred_sets[tool]])
                       for tool in pred_sets}
        return result_sets

    def take_custom_results(self, inputfiles, result_sets={}):
        """Takes a list of inputfiles and extracts the results from them to
           include them in the consensus with the tools of CAMI

        :param inputfiles: A list of dictionaries with the following properties:
                           key: The used tool name
                           values: the paths to result files of these tools
        :type inputfiles: list(dict)
        :return: A dictionary that saves the predicted vertices with respect
                 to the corresponding tool
        :rtype: dict(AlgorithmWrapper():set(Graph.vertex()))
        """
        for tool in inputfiles:
            result_list = []
            with open(inputfiles[tool]) as rfile:
                for idx, line in enumerate(rfile):
                    if idx == 0:
                        tool.name = line.strip()
                        self.code2toolname[tool.code] = tool.name
                    else:
                        node = line.strip()
                        if node in self.ppi_gene2vertex:
                            result_list.append(self.ppi_gene2vertex[node])
                result_sets[tool] = set(result_list)
        return result_sets

    def create_consensus(self, result_sets, save_output=True):
        """takes a set of active module predictions and creates a consensus
           that combines all the results of the different tools.

        :param result_sets: A dictionary with the following properties:
                          key: The used tool as AlgorithmWrapper() Object
                          values: Set of vertices in the ppi_graph that were
                                  predicted by the key-tool
        :type result_sets: {AlgorithmWrapper(): {Graph().vertex()}}
        """
        # calculate gene weights
        # set of all result genes 
        cami_scores = self.ppi_graph.vertex_properties["cami_score"]
        predicted_by = self.ppi_graph.vertex_properties["predicted_by"]
        # consens_threshold = min(self.nof_tools, 2)
        ppi_graph = self.ppi_graph
        seed_list = self.seed_lst
        tool_name_map = self.code2toolname
        gene_name_map = self.ppi_vertex2gene

        # remove seeds from result sets
        for tool in result_sets:
            result_sets[tool] -= set(self.seed_lst)

        params_0 = {'function': {'union':cami_v1.make_union, 
                                 'intersection':cami_v1.make_intersection,
                                 'first_neighbours': cami_v1.make_first_neighbor_result_set}
                    }

        params_tr1 = {'hub_penalty': [0.5],
                     'damping_factor': [0.75],
                     'confidence_level': [0.8],
                     'ranking': ['trustrank'],
                     'function': {'cami_v2': cami_v2.run_cami}}
       
        params_tr2 = {'hub_penalty': [0.5],
                     'damping_factor': [0.85],
                     'confidence_level': [0.65],
                     'ranking': ['trustrank'],
                     'function': {'cami_v3': cami_v3.run_cami}
       }
        cami_setting_list = generate_param_combinations(params_0)+\
                            generate_param_combinations(params_tr1)+\
                            generate_param_combinations(params_tr2)
                            
        camis = {}
        for setting in cami_setting_list:
            if setting[1]:
                func_name = setting[0] + '_' + setting[1].rsplit('_', 1)[-1]
            else:
                func_name = setting[0]

            camis[func_name] = setting[2]

        # transform all vertex indices to their corresponding gene names in a result set
        for tool in result_sets:
            self.result_gene_sets[tool.name] = set([gene_name_map[vertex] for vertex in result_sets[tool]])

        # create integer codes for cami_versions (needed for predicted_by vertex property)
        recursion_limit = sys.getrecursionlimit()
        for cami_method_name, cami_params in camis.items():
            if self.debug:
                print("Running " + cami_method_name)
            # create integer codes for cami_versions (needed for predicted_by vertex property)
            tool_code = max(list(tool_name_map.keys())) + 1
            tool_name_map[tool_code] = cami_method_name

            cami_vertices, putative_vertices, codes2tools = cami_params['function'](result_sets, ppi_graph, seed_list,
                                                                                    predicted_by, cami_scores,
                                                                                    tool_name_map, tool_code,
                                                                                    cami_params['params'])
            # sort the resulting vertices according to their cami_score
            cami_vlist = sorted(cami_vertices, key=lambda v: cami_scores[v], reverse=True)

            seed_genes = [self.ppi_vertex2gene[seed_vertex] for seed_vertex in seed_list]
            # translate the resulting vertex() ids to the corresponding names in the ppi network
            cami_genes = [self.ppi_vertex2gene[cami_vertex] for cami_vertex in cami_vlist]

            if self.debug:
                print(f'With the given seed genes: {seed_genes} \n' +
                      f'CAMI ({cami_method_name}) proposes the following genes to add to the Active Module (sorted by CAMI Score):')
                for vertex in cami_vlist:
                    print(f'{gene_name_map[vertex]}\t{cami_scores[vertex]}\t{codes2tools[vertex]}')
            else:
                print(
                    f'With the {len(seed_genes)} seed genes CAMI ({cami_method_name}) proposes {len(cami_vlist)} genes to add to the Active Module')

            # for visualization with nvenn
            self.result_gene_sets[cami_method_name] = set(cami_genes)

            sys.setrecursionlimit(recursion_limit)
            # save the results in outputfiles
            if save_output:
                self.generate_output(cami_method_name, seed_genes, cami_vlist, cami_vertices, putative_vertices,
                                     cami_genes,
                                     gene_name_map, codes2tools, cami_scores)

        # add seeds to result sets for drugstone and digest
        for toolname in self.result_gene_sets:
            self.result_module_sets[toolname] = self.result_gene_sets[toolname].union(
                set([gene_name_map[svertex] for svertex in self.seed_lst]))
            print(
                f'With the {len(seed_genes)} seed genes the module predicted by {toolname} contains {len(self.result_module_sets[toolname])} genes')

    def generate_output(self, cami_method, seed_genes, cami_vlist, cami_vertices, putative_vertices, cami_genes,
                        gene_name_map, codes2tools, cami_scores):
        # save all predictions by all tools
        if self.debug:
            print('Saving the results...')
        with open(f'{self.output_dir}/all_predictions_{self.uid}.tsv', 'w') as outputfile:
            outputfile.write(
                f'CAMI predictions with {len(self.seed_lst)} of initially {len(self.initial_seed_lst)} seeds: {seed_genes},\n' +
                f'initially: {self.initial_seed_lst}\n')
            outputfile.write(f'gene\tpredicted_by\tcami_score\tindex_in_graph\tdegree_in_graph\n')
            all_vertices = cami_vertices.union(putative_vertices)
            for vertex in all_vertices:
                outputfile.write(
                    f'{gene_name_map[vertex]}\t{codes2tools[vertex]}\t{cami_scores[vertex]}\t{str(vertex)}\t{vertex.out_degree()}\n')
        if self.debug: print(
            f'saved all predictions by the used tools in: {self.output_dir}/all_predictions_{self.uid}.tsv')

        # save the predictions made by cami
        ncbi_url = ('\tncbi_url' if self.ncbi else '')
        ncbi_summary = ('\tncbi_summary' if self.ncbi else '')

        with open(f'{self.output_dir}/{cami_method}_output_{self.uid}.tsv', 'w') as outputfile:
            outputfile.write(f'gene\tindex_in_graph\tcami_score\tdegree_in_graph{ncbi_url}{ncbi_summary}\n')
            for vertex in cami_vlist:
                if self.ncbi:
                    url, summary = ncbi.send_request(gene_name_map[vertex])
                    url = '\t' + url
                    if summary is not None:
                        summary = '\t' + summary
                    else:
                        summary = ''
                else:
                    url, summary = '', ''
                outputfile.write(
                    f'{gene_name_map[vertex]}\t{str(vertex)}\t{cami_scores[vertex]}\t{vertex.out_degree()}{url}{summary}\n')

        # save predicted modules by all other tools
        for tool in self.result_module_sets:
            with open(f'{self.output_dir}/{tool}_output_module_{self.uid}.tsv', 'w') as outputfile:
                outputfile.write('gene\n')
                for gene in self.result_gene_sets[tool]:
                    outputfile.write(f'{gene}\n')
            if self.debug:
                print(f'saved {tool} output in: {self.output_dir}/{tool}_output_{self.uid}.tsv')

    def visualize_and_save_comparison_matrix(self, additional_id='',
                                             title='Intersections of result_gene_sets of all analyzed algorithms.'):
        """Create a comparison matrix of the results of all tools. And save it as png file.
        """
        identifier = f'{self.uid}_{additional_id}' if additional_id else self.uid
        comp_matrix = comparison_matrix.make_comparison_matrix(self.result_gene_sets)
        comp_fig, comp_ax, norm_fig, norm_ax = comparison_matrix.plot_comparison_matrix(comp_matrix,
                                                                                        title=title,
                                                                                        n_rows=self.nof_tools)
        comp_fig_file = f'{self.output_dir}/comparison_matrix_{identifier}.png'
        comp_fig.savefig(comp_fig_file, bbox_inches="tight")
        if self.debug:
            print(f'saved comparison matrix in: {comp_fig_file}')
        norm_fig_file = f'{self.output_dir}/comparison_matrix_{identifier}_normalized.png'
        if self.debug:
            print(f'saved normalized comparison matrix in: {norm_fig_file}')
        norm_fig.savefig(norm_fig_file, bbox_inches="tight")
        plt.close(comp_fig)
        plt.close(norm_fig)
        return comp_fig_file, norm_fig_file

    def use_nvenn(self, download=False):
        """Create Venn Diagrams via a external tool named nvenn by degradome.
           Sends a request via requests to the degradome server.
           Returns the URL of the result.
        """
        # visualize with degradome
        n_venns = len(self.result_gene_sets)
        if n_venns < 7:
            degradome_sets = {tool: self.result_gene_sets[tool]
                              for tool in self.result_gene_sets
                              if len(self.result_gene_sets[tool]) > 0}
            url = degradome.send_request(degradome_sets)
            with open(f'{self.output_dir}/venn_link_{self.uid}.txt', 'w') as f:
                f.write(url)
            if download:
                self.download_diagram(url)
            return url
        else:
            print('Cannot use degradome to create venn diagrams of 6 or more tools')
            return None

    def download_diagram(self, url):
        venn_name = f'{self.output_dir}/vdiagram_{self.uid}'
        response = degradome.download_image(url, venn_name + '.png')
        if response is not None:
            with open(f'{venn_name}.html', 'w') as r:
                r.write(response.html.html)

    def use_drugstone(self):
        symbol = self.ppi_graph.vertex_properties["symbol"]
        cami_module = self.cami_module
        cami_symbols = [symbol[vertex] for vertex in cami_module]
        cami_symbols.append
        cami_symbol_edges = []

        for vertex in self.cami_vertices:
            for edge in vertex.all_edges():
                cami_symbol_edges.append((symbol[edge.source()], symbol[edge.target()]))
        # print(list(set(cami_symbol_edges)))
        url = drugstone.send_request(cami_symbols, cami_symbol_edges)
        print(f'You can find a network visualization of the CAMI module via: {url}')
        with open(f'{self.output_dir}/drugstone_link_{self.uid}.txt', 'w') as f:
            f.write(url)
        return url

    def remove_seeds(self, idx_lst):
        """remove seeds at indices idx

        Args:
            idx_lst (lst): list of indices to be removed
        """
        removed_seeds = [self.seed_lst[idx] for idx in idx_lst]
        self.seed_lst = [seed for seed in self.seed_lst if seed not in removed_seeds]
        for seed in self.seed_lst:
            self.ppi_graph.vertex_properties["cami_score"][seed] = self.seed_score
        for seed in removed_seeds:
            self.ppi_graph.vertex_properties["cami_score"][seed] = 0.0
        return removed_seeds
