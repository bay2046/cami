import os
import uuid
import sys
import graph_tool
from configparser import ConfigParser
from algorithms.DiamondWrapper import DiamondWrapper
from algorithms.DominoWrapper import DominoWrapper
from algorithms.RobustWrapper import RobustWrapper
from algorithms.HHotNetWrapper import HHotNetWrapper
from algorithms.CustomWrapper import CustomWrapper


def all(ppi_file, seed_file,
        config='camiconf',
        cami_tools=['diamond', 'domino', 'robust'], 
        ext_results=[],
        symbol_column=None, delimiter="\t"):
    """executes all preprocessing steps, returns a ppi graph, a list of seeds in the network, the initial 
       list of seeds and a list of initialized tools
    :param ppi_file: path to the ppi file
    :type ppi_file: str
    :param seed_file: path to the seed file
    :type seed_file: str
    :param tools: list of tools to be executed by CAMI, defaults to ['diamond', 'domino', 'robust']
    :type tools: list, optional
    :param ext_results: list paths to external results, defaults to []
    :type ext_results: list, optional
    :param symbol_column: column name of the symbol column, defaults to None
    :type symbol_column: str, optional
    :param delimiter: delimiter in input csv, defaults to "\t"
    :type delimiter: str, optional
    :return: Graph() object, list of seeds in network, initial list of seeds, list of initialized tools
    :rtype: Graph(), list, list, list
    """
    g = csv2graph(ppi_file, symbol_column, delimiter)
    seeds = txt2lst(seed_file)
    checked_seeds = update_seed_list(seeds, g)
    cami_wrappers = initialize_cami_tools(cami_tools,config)
    ext_wrappers = initialize_external_tools(ext_results,config,len(cami_tools))
    all_wrappers = cami_wrappers + ext_wrappers
    return g, checked_seeds, seeds, all_wrappers

def create_output_directories(output_dir, tmp_dir, identifier, path_to_data=None):
    """creates the output directories for the results and the temporary files

    :param identifier: identifier for the output directory
    :type identifier: str
    :param output_dir: path to the output directory
    :type output_dir: str
    :param tmp_dir: path to the temporary directory
    :type tmp_dir: str
    """
    if identifier == None and output_dir == None and tmp_dir == None and path_to_data == None:
        print('Please specify an output directory or an identifier and a path to where the results should be stored.')
    if identifier == None:
        identifier = str(uuid.uuid4())
    
    if path_to_data == None:
        path_to_data = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../data')

    if output_dir==None:
        output_dir = os.path.join(path_to_data, f'{identifier}')
        output_dir = os.path.abspath(output_dir)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
    
    if tmp_dir == None:
        tmp_dir = os.path.join(path_to_data, f'tmp/{identifier}')
            
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    return output_dir, tmp_dir
    
def initialize_external_tools(ext_tool_files,config,n_cami_tools):
    ext_wrappers = []
    if len(ext_tool_files) > 0:
        for idx in range(len(ext_tool_files)):
            dummy_wrapper = CustomWrapper(config)
            dummy_wrapper.code = idx + 1 + n_cami_tools
            dummy_wrapper.name = f'ext_tool_{idx}'
            ext_wrappers.append(dummy_wrapper)
    return ext_wrappers

def initialize_cami_tools(tools, config='camiconf'):
    """initializes the that should be executed by CAMI, returns a list of CAMI AlgorithmWrappers

    :param tools: list of tools
    :type tools: list
    :return: list of initialized tools
    :rtype: list
    """
    wrappers = {'diamond': DiamondWrapper(config),
                'domino': DominoWrapper(config),
                'robust': RobustWrapper(config),
                'hotnet': HHotNetWrapper(config)
                }
    tool_wrappers = [wrappers[tool] for tool in tools]
    return tool_wrappers

def update_seed_list(seeds, g):
    """returns an updated version (copy) of the seed list to only contain genes that are in the graph
       also renames the checked seeds to their index of the gene in the graph
    :param seeds: list of seeds
    :type seeds: list
    :param g: graph_tool object Graph()
    :type g: Graph()
    :return: updated list of seeds
    :rtype: list
    """
    name2index = name2index_dict(g)
    updated_seeds = [name2index[seed] for seed in seeds if seed in name2index]
    return updated_seeds

def csv2graph(inputfile, 
              symbol_columns,
              delimiter="\t", nof_tools = 4):
    """transforms a csv-file to a Graph() object

    :param inputfile: inputfile in csv-format
    :type inputfile: str
    :param delimiter: delimiter in input csv, defaults to "\t"
    :type delimiter: str, optional
    :return: Graph() object
    :rtype: Graph()
    """
    print('Creating the PPI network graph and seed list...')
    g = graph_tool.load_graph_from_csv(inputfile, skip_first=True,
        csv_options={'delimiter':delimiter, 'quotechar': '"'})

    if symbol_columns is not None:
        if len(symbol_columns) == 0:
            symbol_columns = ['Official_Symbol_Interactor_A', 'Official_Symbol_Interactor_B']
        symbol_columns = tuple(symbol_columns)
        g.vertex_properties["symbol"] = g.new_vertex_property('string', val='')
        symbol_s, symbol_t = symbol_columns
        unseen_vertices = g.num_vertices()
        for s,t,sym_s,sym_t in g.iter_edges(eprops=[g.edge_properties[symbol_s], g.edge_properties[symbol_t]]):
            if g.vertex_properties["symbol"][s] == '':
                g.vertex_properties["symbol"][s] = sym_s
                unseen_vertices -= 1
            if g.vertex_properties["symbol"][t] == '':
                g.vertex_properties["symbol"][t] = sym_t
                unseen_vertices -= 1
            if unseen_vertices == 0:
                break
    g.vertex_properties["cami_score"] = g.new_vertex_property("float", val=0.0)
    values = (255) * [-1]
    g.vertex_properties["predicted_by"] = g.new_vertex_property("vector<int16_t>", val=values)
    return g

def txt2lst(seed_file):
    """transforms a \n delimitered textfile of seeds to a list of strings

    :param seed_file: path to the desired input file where each line contains
                      one seed gene, if there are multiple columns, the node ID
                      has to be in the first line and the values need to be tab
                      separated
    :type seed_file: str
    :return: list of seeds
    :rtype: lst(str)
    """

    seeds = []
    with open(seed_file) as file:
        for line in file.readlines()[1:]:
            seeds.append(line.split("\t")[0].strip())
    return seeds

def index2name_dict(g):
    """translates an index of a vertex to its corresponding name in a given 
       Graph() object

    :param vertex: name of a vertex in the Graph()
    :type vertex: str
    :param g: graph_tool object Graph()
    :type g: Graph()
    """
    return g.vertex_properties["name"]

def name2index_dict(g):
    """creates a dictionary which translates a name of a vertex to its 
       corresponding index in a given Graph() object

    :param vertex: name of a vertex in the Graph()
    :type vertex: str
    :param g: graph_tool object Graph()
    :type g: Graph()
    """
    index2name = g.vertex_properties["name"]
    return {index2name[v]:v for v in g.vertices()}
        
