#!/usr/bin/env python3
import os
import sys
import shutil
import preprocess
import cami_suite
import argparse
import webbrowser
import evaluation_scripts.seed_variation_script as sv

def main(ppi_network, seeds, tools, tool_weights, consensus, evaluate,
         output_dir, identifier, save_temps, nvenn, save_image, force, drugstone, ncbi, configuration, 
         seed_variation, parallelization, external_results, external_only, debug, comparison_matrix, visualize):
    print('CAMI started')
    # preprocess the inputfiles
    if external_only:
        ppi_graph, checked_seed_lst, initial_seed_lst, tool_wrappers = preprocess.all(ppi_network, seeds, configuration, [], external_results)
    else:
        ppi_graph, checked_seed_lst, initial_seed_lst, tool_wrappers = preprocess.all(ppi_network, seeds, configuration, tools, external_results)
    
    if debug:
        print('Created the PPI-network Graph and seed list.')
    n = len(checked_seed_lst)
    m = len(initial_seed_lst)
    if n != m  and not force:
        name = ppi_graph.vertex_properties["name"]
        print(f'There are {abs(m-n)} seeds missing in the PPI network: ({set([name[cseed] for cseed in checked_seed_lst]).symmetric_difference(set(initial_seed_lst))})')
        choice = input('Continue? [y/n]')
        if choice == 'y':
            pass
        else:
            print('Aborted because of missing seeds. Please try again.')
            exit(1)

    if debug: print(f'Continuing with vertices at indices {[int(seed) for seed in checked_seed_lst]} as input seeds')

    # change directory to ~/cami/cami_src (home of cami.py)
    cami_home = os.path.dirname(os.path.abspath(__file__))
    
    os.chdir(cami_home)
    home_path = os.path.dirname(os.getcwd())
    if debug:
        print(f"Home directory of cami: {home_path}")

    # initialize CAMI
    cami = cami_suite.cami(ppi_graph,checked_seed_lst,tool_wrappers,home_path,
                           initial_seed_lst=initial_seed_lst,uid=identifier, output_dir=output_dir,
                           configuration=configuration,
                           parallelization=parallelization, ncbi=ncbi, debug=debug,
                           save_temps=save_temps, )
    
    if tool_weights is not None:
        cami.initialize_tool()
    
    if consensus or (not consensus and not evaluate) or seed_variation:
        result_sets = cami.make_predictions()
        cami.create_consensus(result_sets)
        # adds the seeds to the results, right now result_sets contains the seeds that should be ADDED to the module
        # for result in result_sets.keys():
        #     result_sets[result] = result_sets[result].union(cami.seed_lst)
        if nvenn or save_image or visualize:
            print('Sending results to nVenn')
            url = cami.use_nvenn()
            if url:
                if nvenn:
                    webbrowser.open(url)
                if save_image:
                    cami.download_diagram(url)
        
        if visualize or comparison_matrix or seed_variation:
            cami.visualize_and_save_comparison_matrix()
        
        if drugstone is not None:
            print('Sending results to DrugstOne')
            cami.use_drugstone()
            
        if consensus:
            cami.reset_cami()

    if evaluate and (consensus or seed_variation):
        cami.make_evaluation()
        
    # SEED VARIATION
    if seed_variation:
        cami = sv.make_seedvariation(cami, seed_variation, removal_frac=0.2, vis=True)
    
    tmp_dir = cami.tmp_dir
    
    if save_temps:
        print(f'All temporary files were kept in {tmp_dir}')
        print(f'You can remove them by typing: rm -rv {tmp_dir}')
        print('Please note that CAMI internally works with the indices of the' +
               ' vertices in the PPI-Network. This means that the names in the'+
               ' inputfiles of the tools do not correspond to the gene names ' +
               'but to their indices in the graph_tool Graph that CAMI creates.'
               )
    else:
        print(f'Removing all temporary files in {tmp_dir}...')
        shutil.rmtree(tmp_dir)
        print('Done.')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="CAMI has two functions:\n" +\
        "Consensus Prediction:\nUse different algorithms to find " +\
        "active disease modules in a given PPI-network and combines their results. "+\
        "Uses a protein-protein-interaction-network (PPI-network) and a seed list as input\n"+\
        "Evaluation:\nEvaluate different tools with respect to the consensus of multiple tools."+\
        "If no mode is specified both functions are excecuted.")
    parser.add_argument('-n', '--ppi_network', required=True, action='store',
            help="Path to a tsv file containing the edges of the base PPI in the first two columns")
    parser.add_argument('-s','--seeds', required=True, action='store',
            help="Path to a txt file containing the seeds delimitered by breakline characters")
    parser.add_argument('-t', '--tools', nargs='+', action='store', default=["diamond", "domino", "robust"],
            help="List of tools that the user wants to use for prediction.\n Available tools are:\n" + \
                "domino\ndiamond\nrobust\hotnet\nThe default tools are: diamond, domino and robust.")
    parser.add_argument('-ext', '--external_results', nargs='+', action='store', default=[],
            help="List of paths to results generated by external tools. The files should contain the tool name in the first line and then the predicted genes (1 gene per line)")
    parser.add_argument('-ext_only', '--external_only', nargs='+', action='store', default=[],
            help="List of paths to results generated by external tools. The files should contain the tool name in the first line and then the predicted genes (1 gene per line). Only the external results will be used for consensus prediction.")
    parser.add_argument('-w', '--tool_weights', nargs='+', action='store', default=None,
            help="List of weights for the tools. If you have [domino, diamond, robust] as list of tools and diamonds weight should be twice as high as the other tools type: 1 2 1")
    parser.add_argument('-c', '--consensus', action='store_true', help="run only the consensus prediction part of cami")
    parser.add_argument('-var', '--seed_variation', action='store', help="repeat consensus selection multiple times (please provide the number of iterations) while removing 20 percent of the seeds.")
    parser.add_argument('-e', '--evaluate', action='store_true', help="evaluation using DIGEST", default=False)
    parser.add_argument('-o', '--output_dir', action='store', help="path to output directory", default=None)
    parser.add_argument('-id', '--identifier', action='store', help="ID for the current excecution of cami. Defaults to a randomly generated ID")
    parser.add_argument('-tmp', '--save_temps', action='store_true', help="keep all temporary files")
    parser.add_argument('-v', '--visualize', action='store_true', help="Visualize the results with nVenn, comparison matrix and drugstone")
    parser.add_argument('-comp', '--comparison_matrix', action='store_true', help="Visualize the results with a comparison matrix")
    parser.add_argument('-nv', '--nvenn', action='store_true', help="Visualize results using nVenn by Degradome, an external webtool. Please note that nvenn can only be used for visualization with up to 5 tools.")
    parser.add_argument('-img', '--save_image', action='store_true', help="Save the venn diagram and/or comparison matrix from the visualization functions as png.")
    parser.add_argument('-f', '--force', action='store_true', help="Ignore warnings and overwrite everything when excecuting CAMI.")
    parser.add_argument('-d', '--drugstone', nargs='*', action='store', default=None,
                        help="Visualize the cami module via the drugstone API. If necessary the user needs to provide a list of the two titles of the two columns that contain the symbols of the gene edges in the inputfile of the PPI network. The order needs to correspond to the order of the first two columns. The default is 'Official_Symbol_Interactor_A Official_Symbol_Interactor_B'. Please note that the symbol columns cannot be the first two columns. If the two main edges in the first two columns are correspond also the gene symbols please duplicate these columns.")
    parser.add_argument('-ncbi', '--ncbi', action='store_true',
                        help="Save the NCBI URLs and Summaries of the genes in the CAMI output.")
    parser.add_argument('-conf', '--configuration', nargs='*', action='store', default='camiconf',
                        help="Choose a configuration for the static variables.")
    parser.add_argument('-p', '--parallelization', action='store_true', 
            help="run the tools for prediction parallelized")
    parser.add_argument('-db', '--debug', action='store_true', 
            help="run CAMI with verbose outputs")
    #TODO List with additional arguments if needed by certain tools

    args = vars(parser.parse_args())
    if not os.path.exists(args['ppi_network']):
        raise RuntimeError('File does not exist: ' + args['ppi_network'])

    if not os.path.exists(args['seeds']):
        raise RuntimeError('File does not exist: ' + args['seeds'])

# add name for implemented tools here:
    implemented_tools = [
                         "domino",
                         "diamond",
                         "robust",
                         "hotnet"
                         ]
    if args['tools']:
        for tool in args['tools']:
            if tool not in implemented_tools:
                raise RuntimeError(tool + f' not implemented yet. Implemented tools are: {implemented_tools}')

    main(**args)
