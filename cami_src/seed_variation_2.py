#!/usr/bin/env python3
import sys
from os.path import basename
import subprocess, random
import matplotlib.pyplot as plt

network = sys.argv[1]
seedfiles = sys.argv[2:]
config = 'seed_variationconf'
for seeds in seedfiles:
    identifier = basename(seeds).rsplit('.')[0] + '_seedvar_different_consensus_approaches'
    command = f'./cami.py -n {network} -s {seeds} -id {identifier} -conf {config} -var 100 -f -p;'
    subprocess.call(command, shell=True)
